package com.rmd.sains.model;

import lombok.Data;

import java.math.BigDecimal;

public class Total {

    private BigDecimal gross;

    public Total( BigDecimal gross ){
        this.gross = gross;
    }

    public BigDecimal getGross(){
        return gross;
    }

    public BigDecimal getVat(){
        return BigDecimal.ONE;
    }
}
