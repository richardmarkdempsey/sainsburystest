package com.rmd.sains.model;

import lombok.Data;

import java.util.List;

@Data
public class ProductSummary {

    public List<Product> results;
    public Total total;
}
