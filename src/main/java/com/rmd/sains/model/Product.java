package com.rmd.sains.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.math.BigDecimal;

@Data
public class Product {
    String title;
    String description;
    @JsonProperty("unit_price")
    BigDecimal unitPrice;
    @JsonProperty("kcal_per_100g")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Integer calories;

    public static Product parse( Element element ){

        Product product = null;
        try {
            product = new Product();

            product.title = parseProductName(element);



            Document doc = Jsoup.connect(getProductLink(element)).get();

            product.description = parseDescription(doc);

            String unitPrice = parseUnitPrice(doc);
            product.unitPrice = new BigDecimal( unitPrice );

            product.calories = parseCalories(doc);

        }
        catch( Exception ex ){
            ex.printStackTrace();
            product = null;
        }

        return product;
    }

    public static String parseProductName(Element element ) {
        String name = null;

        Element link = element.select("a").first();

        name = link.text().trim();

        return name;
    }

    public static String parseDescription(Document doc ) {

        return doc.select("div.productText")
                .first().select("p")
                .first().text().trim();
    }

    public static String parseUnitPrice(Document doc  ) {

        String unitPrice =  doc.select("p.pricePerUnit")
                .first().text().trim().substring(1);

        return unitPrice.substring(0, unitPrice.indexOf("/unit"));
    }

    public static Integer parseCalories(Document doc  ) {

        Elements elements = doc.select("td.nutritionLevel1");

        if( !elements.isEmpty() ) {
            String calories = elements.first().text().trim();

            return Integer.valueOf(calories.substring(0, calories.indexOf("kcal")));
        }

        return null;
    }

    private static String getProductLink( Element element ){

        String baseUrl = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries";
        Element link = element.select("a").first();

        String relUrl = link.attr("href");

        return baseUrl + relUrl.substring(relUrl.indexOf("/groceries") + "/groceries".length());
    }
}

