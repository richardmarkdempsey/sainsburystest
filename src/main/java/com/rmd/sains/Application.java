package com.rmd.sains;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rmd.sains.model.Product;
import com.rmd.sains.model.ProductSummary;
import com.rmd.sains.model.Total;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class Application {

    private static final String SAINSBURYS_TEST_URL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";


    public static void main(String[] args) {

        try {
            Document doc = Jsoup.connect(SAINSBURYS_TEST_URL).get();

            List<Product> products = doc.select("div.product")
                    .stream()
                    .map(element->Product.parse(element))
                    .filter(product-> product!=null)
                    .collect(Collectors.toList());

            Total totals = new Total(products.stream()
                    .map(product->product.getUnitPrice())
                    .reduce(BigDecimal.ZERO, BigDecimal::add));

            ProductSummary productSummary = new ProductSummary();
            productSummary.setResults( products );
            productSummary.setTotal(totals);

            ObjectMapper objectMapper = new ObjectMapper();

            String jsonResult = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(productSummary);

            System.out.println(jsonResult);
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }
    }

}
